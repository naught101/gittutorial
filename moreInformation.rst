================================
Where to go for more information
================================

This tutorial has been the briefest of introductions to using git.
Hopefully, you have enough to get you started and to encourage you to use a Version
Control System when you are developing software.

If you want to use git as your VCS, you will undoubtably need access
to more detailed information.

The following is a list of references that I have found useful in
using git and preparing this tutorial:

General information
----------------------------------------

* *git help* is always a good place to find out how to use the various git commands.
* The official git_ website.
* `An intro to git`_ by C. Titus Brown. A short tutorial written by one of the Software Carpentry folks.
* bitbucket_ and github_ both provide useful
  tutorials on using git and using their public repository hosting services.
* *Pragmatic Guide to Git* by Travis Swicegood, 2010, ISBN-13 978-1-934356-72-2.
  This is a gentle introduction to using git.
* *Pragmatic Version Control Using Git* Also by Travis Swicegood, 2008, ISBN-13 978-1-934356-15-9.
  This covers using git in more detail.
* *Version Control with Git* by Jon Loeliger, 2009, ISBN-13 978-0-596-52012-0.
  This is a definitive text that gets into the fine details:
  a really good book for someone who wants to become a git *power user*.
* `The git wiki`_
    * Has a nice page of `useful aliases`_

.. _git: http://git-scm.com
.. _bitbucket: https://bitbucket.org
.. _github: https://github.com
.. _An intro to git: http://ged.msu.edu/angus/git-intro.html
.. _The git wiki: https://git.wiki.kernel.org
.. _useful aliases: https://git.wiki.kernel.org/index.php/Aliases

