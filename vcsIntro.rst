========================
What is Version Control?
========================

A version control system is software that partially automates the task of keeping track of changes to documents or code.
There are many different VCSs available, with many technical differences in their implementation.
In this workshop, we will mostly focus on the processes involved in using a VCS, and will only touch on technical structural aspects of different VCSs.

One important distinction between the various VCSs is whether they are *Centralised* or *Distributed*.

Centralised version control
----------------------------------------

Traditionally, a VCS was implemented as a centralised service, i.e. a client-server configuration
where a single repository was hosted by a single server and users (clients) interacted with
the central repository to download and upload changes. This model allowed
multiple users to develop code in a managed way using either file locking or version merging.

Systems that use this approach are ClearCase, SCCS, RCS, CVS, and Subversion.

This approach has some disadvantages:

* Clients must have access to the server to make commits. This can be a problem in that
  a network connection will be needed when the server is hosted on another machine.
* There is a single point of failure.
  If the repository is not available, no commits can be made.
  On large development projects, the server can become a bottleneck for repository users.

The decentralising revolution
----------------------------------------

To overcome these problems, later systems were developed using a distributed approach (peer-peer).
In this model, each user has a complete shareable copy of the repository. A user develops code and makes
commits in isolation. Collaboration is achieved by *cloning* an existing repository, developing in isolation,
and at some point *pushing* or *pulling* the changes to other repositories.

Examples of **Distributed VCS's (DVCS)** using this approach are Mercurial and git.

One popular way of working with a DVCS is to utilise a public repository hosting service such as github or bitbucket.
These services provide an off-site backup, as well as some really nice ways of handling collaboration with unfamiliar users.
Toward the end of this tutorial, we will go through this workflow using the bitbucket public repository.

