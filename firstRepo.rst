==============================
Creating your First Repository
==============================

In this section, you will create your first repository, create a file and put it
under version control.

Creating a repository is very straightforward, either:

* create a new repository in an empty directory,

* create a repository to manage the files in an existing directory, or

* clone a repository from an existing local or remote git repository.

This repository can then manage all the content in this directory and all of its sub-directories.

Create a git repository
=======================

Let's get started by creating a new empty directory called ``myProject`` and initialising a git
repository::
   
   $ mkdir myProject
   $ cd myProject
   $ git init
   Initialized empty Git repository in /home/user/myProject/.git/

That was easy! But what actually happened? All of the git repository internals have been
placed in the subdirectory, ``.git``:: 

   $ ls -a
   .  ..  .git
   
   $ ls -a .git
   .  ..  branches config description HEAD hooks info objects refs
   
You might be wondering, how do I get rid of a git repository? Well, since all of the repository
information is stored in the ``.git`` directory, deleting a repository is a simple as
deleting this directory, (``rm -rf .git``). Note that while this gets rid of the repository
internals, it does not remove any of the content in your working directory (``myProject/``).
It just removes your files from being managed by the git version control system (VCS).

Let's use one of the git commands used for finding out information about
the repository, ``git status``::

   $ git status
   # On branch master
   #
   # Initial commit
   #
   nothing to commit (create/copy files and use "git add" to track)

Create a file and put it under version control
----------------------------------------------
Ok, let's create a file called ``stats.py`` with the following content::

   # Calculate the sum of the values in a text file,
   # formatted as one floating point value per line.
   
   import sys
   
   values = []
   for line in open(sys.argv[1]):
      value = float(line)
      values.append(value)
      
   total = sum(values)
   
   print len(values), 'values were read in'   
   print 'The sum of the input values is:', total
   
If you run ``git status`` you will see that git already knows of the existence of the
file and that it is currently not tracked::

   $ git status
   # On branch master
   #
   # Initial commit
   #
   # Untracked files:
   #   (use "git add <file>..." to include in what will be committed)
   #
   #	stats.py
   nothing added to commit but untracked files present (use "git add" to track)
   

There are two steps to putting a file under version control:

First let git know that you want it to be *tracked* under version control by using ``git add``::
      
   $ git add stats.py
   
   $ git status
   # On branch master
   #
   # Initial commit
   #
   # Changes to be committed:
   #   (use "git rm --cached <file>..." to unstage)
   #
   #	new file:   stats.py
   #
 
This adds the file to the index, or "*stages*" it, ready for commiting.

We then *commit* the staged changes (addition of the file) to the repository::
   
   $ git commit -m"Initial commit of stats.py"
   [master (root-commit) aaad9ce] Initial commit of stats.py
    1 files changed, 14 insertions(+), 0 deletions(-)
    create mode 100644 stats.py

The ``-m`` option is a convenient way to add a short message to the repository log describing
what is in the commit. If you run ``git commit stats.py``, without the ``-m`` option, your preferred
editor will be started and you can add your commit message, and optionally add extra notes.

Let's look at the repository status and introduce a new command, ``git log``
that enables us to view the history of changes to the repository::

   $ git status
   # On branch master
   nothing to commit (working directory clean)
   
   $ git log
   commit aaad9ced7a33f5f8dc301411c2958f0267cfd82c
   Author: Your Name <your.name@yourdomain.com>
   Date:   Tue Feb 5 16:35:40 2013 +1100

    Initial commit of stats.py

Here you can see from ``git status`` that all of the changes to the tracked files have
been committed to the repository and ``git log`` lists the last commit(s) that have been made.

Put an existing project under version control
=============================================
Here, we take an existing project in directory and create a repository for it, and add all files from that directory.
The process, in this case is almost the same as in the previous example::

   $ cd <to the top directory of the existing project you want git to manage>
   $ git init
   $ git add .    # Adds everything in the current directory and subdirectories.
   $ git commit -m"your commit message"

Clone an existing repository
============================
The following command will create a new directory, ``gitTutorial`` containing a clone
of the specified git repository (this can be either a local or remote repository)::

   $ cd <to where you want the repository placed>
   
   $ git clone https://bitbucket.org/mrezny/gittutorial.git gitTutorial
   Cloning into gitTutorial...
   Password: 
   remote: Counting objects: 46, done.
   remote: Compressing objects: 100% (45/45), done.
   remote: Total 46 (delta 7), reused 0 (delta 0)
   Unpacking objects: 100% (46/46), done.
   
If you have sphinx installed, you can now compile the tutorial with ``make html``, and point your browser to
``file:///<where you put your repository>/gitTutorial/_build/html/index.html``   
and you will be able to view a local copy of this tutorial.

Notice, that we have already learnt to use seven git commands:

* help
* init
* status
* add
* commit
* log
* clone

