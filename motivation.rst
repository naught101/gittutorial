====================================
Motivation for using Version Control
====================================

When working with software, or writing documents, it is prudent to have a back-up strategy, as well as away of tracking changes, and some way of sharing your work with collaborators.

There are many ways in which each of these three processes can be done. However, software developers
generally use purpose-built tools that can support all three: **Version Control Systems (VCS)**.

We will use VCS throughout this course, but be aware a VCS is known by many other names such as:
revision control system (RCS), version control, source control, and other permutations of
revision, version, code, content, control, management, and system.

Main advantages of using a VCS:
----------------------------------------

* **Revision tracking**: As you develop software, a VCS allows you to keep track of your progress and easily
  retrieve previous versions. This is especially useful as you debug problems, develop new algorithms
  and functionality.

* **Sharing**: As the complexity of software increases, it is more unlikely that you will develop
  software in isolation.
  A VCS allows you to easily share your software with others, allows you to easily access software
  from other people, and provides a platform on which to do collaborative development.

  * **Collaborative writing**: A VCS can also be used for collaboratively writing documents in plain text formats (e.g. LaTeX, Markdown, RST).

* **Back-up strategy**: Data can be easily lost either accidently or by hardware failures.

* **Manage multiple version**:
  As you develop, features may need to be implemented that disrupt the project.
  Using branches, you can work on developing such features, while maintaining a stable, working main version.
  Branches can later be merged into the main version, when they are complete.

