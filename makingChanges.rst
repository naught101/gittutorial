----------------------------
Making changes to your files
----------------------------

In this section we will learn how to use git to keep track of changes to your project files.

Modify ``stats.py`` to also calculate and print the average of the input values::

   :
   print 'The sum of the input values is:', total
   print 'The average of the input values is:', total / len(values)

Let's see what git can tell us about changes::

   $ git status
   # On branch master
   # Changes not staged for commit:
   #   (use "git add <file>..." to update what will be committed)
   #   (use "git checkout -- <file>..." to discard changes in working directory)
   #
   #	modified:   stats.py
   #
   no changes added to commit (use "git add" and/or "git commit -a")

As you can see ``git status`` picks up that ``stats.py`` has been modified. However, it might be
useful to see what has actually been changed in this file. For this we can use ``git diff``::

   $ git diff
   diff --git a/stats.py b/stats.py
   index 540410c..16b3bd5 100644
   --- a/stats.py
   +++ b/stats.py
   @@ -12,3 +12,4 @@ total = sum(values)

    print len(values), ' Values were read in'
    print 'The sum of the input values is:', total
   +print 'The average of the input values is:', total / len(values)

This output shows that an extra line has been added to the working copy of the file, ``stats.py``.
Once we are happy we can then commit the change to the repository.

Like adding the original file, committing changes is a two-part process.
First we add the changes that we want included in the next commit,
then we do the commit. Integral to this process is using ``git status`` to ensure we are adding and
committing what we actually want.

From the previous status, we see that there is one modified file, ``stats.py``.
The first step is to add the change and to then look at the status::

   $ git add stats.py

   $ git status
   # On branch master
   # Changes to be committed:
   #   (use "git reset HEAD <file>..." to unstage)
   #
   #	modified:   stats.py
   #

You can see that the status has now changed to show that the change is now in the staged index, and ready to be committed.
You can view the changes in the index by using the ``--cached`` option to ``git diff``::

   $ git diff --cached
   diff --git a/stats.py b/stats.py
   index 540410c..16b3bd5 100644
   --- a/stats.py
   +++ b/stats.py
   @@ -12,3 +12,4 @@ total = sum(values)

    print len(values), ' Values were read in'
    print 'The sum of the input values is:', total
   +print 'The average of the input values is:', total / len(values)

Let's go ahead and make the commit and see how the status has changed::

   $ git commit -m"modified stats.py to output the average as well"
   [master 9a202a6] modified stats.py to output the average as well
    1 files changed, 1 insertions(+), 0 deletions(-)

   $ git status
   # On branch master
   nothing to commit (working directory clean)

The status now shows that the ``HEAD`` of the branch and the ``working directory`` are now the same.

In the final part of this section we will look at the log to see the list of changes we have made to our
repository::

   $ git log
   commit 9a202a66e7b2b93b12190dd0b56b594a60ebed22
   Author: Your Name <your.name@yourdomain.com>
   Date:   Thu Feb 7 11:36:27 2013 +1100

       modified stats.py to output the average as well

   commit c6f2746af72c45d6f92741f51b5f4ae1241b79e2
   Author: Your Name <your.name@yourdomain.com>
   Date:   Tue Feb 5 16:36:40 2013 +1100

       put .gitignore under version control

   commit aaad9ced7a33f5f8dc301411c2958f0267cfd82c
   Author: Your Name <your.name@yourdomain.com>
   Date:   Tue Feb 5 16:35:40 2013 +1100

       Initial commit of stats.py

As you would expect, we now see three log entries: one for the initial commit, one for adding .gitignore,
and one for our recent modification.

