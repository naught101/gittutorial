=======
Merging
=======

At this point, on the *std_dev* branch, you should have a version of ``stats.py`` that correctly
calculates and prints out the standard deviation of the input data.

The final step is to merge the changes in the *std_dev* branch into the master. Once this is done,
checking out the master branch will give you the latest version of the code.

Here is how we do this. First we checkout the master branch, then we use ``git merge`` to apply
all the changes in this branch to the version in the master branch::

   $ git checkout master
   Switched to branch 'master'
   
   $ git merge std_dev
   Updating 9a202a6..9b7129e
   Fast-forward
    stats.py |   13 +++++++++++--
    1 file changed, 11 insertions(+), 2 deletions(-)
    
   $ git branch
   * master
     std_dev
     
You will see that we still have two branches. If we checkout either branch and look at the logs you will
see they both have the same sequence of commits::

   $ git log --decorate
   commit 9b7129e86390fcca6a97dc2c8102014e63f5a646 (HEAD, std_dev, master)
   Author: Your Name <your.name@yourdomain.com>
   Date:   Fri Feb 8 20:40:56 2013 +1100
   
       modified stats.py to also calculate and print out the standard deviation
   
   commit 9a202a66e7b2b93b12190dd0b56b594a60ebed22 (tag: sum_and_average)
   Author: Your Name <your.name@yourdomain.com>
   Date:   Thu Feb 7 11:36:27 2013 +1100
   
       modified stats.py to output the average as well
   
   commit c6f2746af72c45d6f92741f51b5f4ae1241b79e2
   Author: Your Name <your.name@yourdomain.com>
   Date:   Tue Feb 5 16:36:40 2013 +1100
   
       put .gitignore under version control
   
   commit aaad9ced7a33f5f8dc301411c2958f0267cfd82c (tag: sum)
   Author: Your Name <your.name@yourdomain.com>
   Date:   Tue Feb 5 16:35:40 2013 +1100
   
       Initial commit of stats.py
       
In fact, we can still do further development independently on either branch if we wish. At some other
point we can then again merge the latest version of std_dev into the master branch.

This type of simple merge, where only the branch being merged into (i.e. master) has no changes is called a "fast forward" merge.
We will see a more complex merge with a conflict in the next section.

At this point, we should also create another tag, ``sum_average_stddev``, to point to the latest version of our project.

The node diagram for our repository should now look like this:

.. image:: images/node_03_merge_std_dev.png
   :align: center

