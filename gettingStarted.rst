Getting Started
===============

First, you will need to check that git is installed on your system::

   $ git --version
   git version 1.7.10.4

Git provides help from the command line::

   $ git help
   usage: git [--version] [--exec-path[=GIT_EXEC_PATH]] [--html-path]
           [-p|--paginate|--no-pager] [--no-replace-objects]
           [--bare] [--git-dir=GIT_DIR] [--work-tree=GIT_WORK_TREE]
           [--help] COMMAND [ARGS]

   The most commonly used git commands are:
      add        Add file contents to the index
      bisect     Find by binary search the change that introduced a bug
      branch     List, create, or delete branches
      checkout   Checkout a branch or paths to the working tree
      clone      Clone a repository into a new directory
      commit     Record changes to the repository
      diff       Show changes between commits, commit and working tree, etc
      fetch      Download objects and refs from another repository
      grep       Print lines matching a pattern
      init       Create an empty git repository or reinitialize an existing one
      log        Show commit logs
      merge      Join two or more development histories together
      mv         Move or rename a file, a directory, or a symlink
      pull       Fetch from and merge with another repository or a local branch
      push       Update remote refs along with associated objects
      rebase     Forward-port local commits to the updated upstream head
      reset      Reset current HEAD to the specified state
      rm         Remove files from the working tree and from the index
      show       Show various types of objects
      status     Show the working tree status
      tag        Create, list, delete or verify a tag object signed with GPG

   See 'git help COMMAND' for more information on a specific command.

Please take some time to become familiar with getting help on using git commands. Note that
this help message explains how to get more detailed help on a specific command.
You can also get more information using ``man git``
or visit the official git website at http://git-scm.com

The next step is to configure git with your name, email address, and your preferred text editor.
These values will be stored globally and will be used by all the git repositories you
create on your workstation.::

   $ git config --global core.editor vim      # or nano, for a simpler, easier to learn editor
   $ git config --global user.name "Your Name"
   $ git config --global user.email "your.name@yourdomain.com"
   $ git config -l
   core.editor=vim
   user.name=Your Name
   user.email=your.name@yourdomain.com

The following command will configure git to use color highlighting when outputting to the terminal::
   
   $ git config --global color.ui auto

If you have successfully got this far you are now ready to move onto the next
session: Creating your first repository.

