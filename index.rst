.. Git Tutorial documentation master file, created by
   sphinx-quickstart on Thu Jan 24 12:54:30 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
.. image:: logo_coecss.jpg
   :align: center
   
Git Tutorial
============
Hi, welcome to the git tutorial.

Contents:
---------

.. toctree::
   :maxdepth: 10

   Motivation <motivation>
   vcsIntro
   gettingStarted
   Your First Repository <firstRepo>
   Ignoring Files <gitignore>
   Making Changes <makingChanges>
   history
   branches
   merging
   conflicts
   Using Bitbucket <bitbucket>
   Git-SVN <gitSVN>
   More information <moreInformation>
   
Authors:
~~~~~~~~
Mike Rezny ( michael.rezny@monash.edu )

Ned Haughton

This tutorial was adapted and expanded from `An intro to git`_ by C. Titus Brown

License: CC-BY-SA_

.. _An intro to git: http://ged.msu.edu/angus/git-intro.html
.. _CC-BY-SA: http://creativecommons.org/licenses/by-sa/2.0/

.. Indices and tables
   ==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
