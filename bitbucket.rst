Sharing Repositories using Bitbucket
==========================================
Up until now, in this tutorial, we have concentrated on keeping track of the history
of our project: revision tracking.
But, in the motivation at the beginning, we saw that there were two other aspects of
software development that a VCS can manage for you as well: backups and sharing.

Public hosting services such as bitbucket_ and github_ provide valuable services that
include backup and sharing.
In this section, you will see some of the things that you can do using bitbucket.
First of all, please ensure you have signed up for an account on bitbucket_.
Go to bitbucket_ and log in.

Clone an Existing Bitbucket Repository
--------------------------------------
You may remember, way back at the start of this tutorial, that we have actually already done this::

   $ cd <to where you want the repository placed>
   $ git clone https://mrezny@bitbucket.org/mrezny/gittutorial.git gitTutorial

In general, if you want to clone a project, go to the project page, e.g. https://bitbucket.org/mrezny/gittutorial
If the repository is public, you will see a clone link on the left:

.. image:: images/bitbucket_gittutorial_clone_link.png
   :align: center
   :scale: 75%

Simply copy the link shown, ``cd`` to the parent directory where you would like to clone the repository and run ``git clone <url>``.
You now have a full copy of the repository, and can view the history of the project, and make your own branches and commits.

Creating a Copy of Your Local Repository on Bitbucket
-----------------------------------------------------
In this section, we will create a repository on bitbucket and import our project to it.
When this has been done, our local repository will be a clone of the one at bitbucket.
Now we have a backup of our repository and the ability to share our work with anyone else.

First step is to create a new repository:

.. image:: images/bitbucket_005_create_menu.png
   :align: center

You should be presented with the following page:

.. image:: images/bitbucket_006_create_page.png
   :align: center
   :scale: 75%

Fill in the form as shown above, ensuring you give your project a name and unchecking
*This is a private repository*.
When you are finished, click  the *Create repository* button, which should take you to the following page:

.. image:: images/bitbucket_007_new_repo.png
   :align: center
   :scale: 75%

Import an Existing Repository
-----------------------------
We are now going to import our existing local repository into the one we created at bitbucket.
Click on *I have code I want to import* and type in or Cut-and-Paste the commands
that come up on the next page:

.. image:: images/bitbucket_008_import_existing.png
   :align: center

The first git command tells our local repository that there is a clone at the bitbucket URL, and gives it the nickname "origin".
The remote repository is currently empty. The second command *pushes* our local changes up to the
remote repository::

   $ git remote add origin https://mrezny@bitbucket.org/mrezny/myproject.git

   $ git push -u origin --all
   Password:
   Counting objects: 21, done.
   Delta compression using up to 4 threads.
   Compressing objects: 100% (19/19), done.
   Writing objects: 100% (21/21), 2.37 KiB, done.
   Total 21 (delta 6), reused 0 (delta 0)
   remote: bb/acl: mrezny is allowed. accepted payload.
   To https://mrezny@bitbucket.org/mrezny/myproject.git
    * [new branch]      master -> master
    * [new branch]      std_dev -> std_dev
   Branch master set up to track remote branch master from origin.
   Branch std_dev set up to track remote branch std_dev from origin.

The ``--all`` option tells git to push all branches to the remote - if you leave this off, ony the current branch will be pushed.

Click on overview, and you should be presented with a page showing your recent activity, including
all the commits you made during this tutorial:

.. image:: images/bitbucket_009_imported_repo.png
   :align: center
   :scale: 75%

In normal use, you continue to use your local repository normally. At regular intervals, you
can *push* your changes up to the remote repository::

   $ git push
   Password:
   Counting objects: 5, done.
   Delta compression using up to 4 threads.
   Compressing objects: 100% (3/3), done.
   Writing objects: 100% (3/3), 352 bytes, done.
   Total 3 (delta 1), reused 0 (delta 0)
   remote: bb/acl: mrezny is allowed. accepted payload.
   To https://mrezny@bitbucket.org/mrezny/myproject.git
      d7396fb..51cc365  master -> master

You can see what branches are available at the remote repository with::

   $ git branch -a
   * master
     std_dev
     remotes/origin/master
     remotes/origin/std_dev

Playing nicely with others
--------------------------
You can optionally give push access to others to your bitbucket repository, or they can give you access to theirs.
If you do, then you might find that they have pushed changes to a branch, that you want.
To pull new changes from upstream, simply run::

   $ git pull

This will fetch all of the new changes, and then automatically fast-forward merge them into your repository, if possible.
Sometimes a fast-forward won't work, for example because there are changes on your branch that were not there when your collaborator last pulled.
In this case, you will have to manually merge using git merge using something like::

   $ git merge origin/master

And then sort out any conflicts manually, as we just did with our Author and License example.

Going further
-------------

Bitbucket provides many more features. But this will be enough to get you started. I recommend
doing the `bitbucket tutorial`_ which shows the next steps which involve how to work more effectively
in a collaborative environment by *forking* repositories and using *pull requests*.

Other online repository hosts, such as Github or Gitorious, follow a very similar pattern, and largely support the same features, so once you get used to one, you should find it fairly easy to use another.

.. _bitbucket: https://bitbucket.org
.. _github: https://github.com
.. _bitbucket tutorial: https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101

